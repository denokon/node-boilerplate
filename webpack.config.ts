import { Configuration } from "webpack";

import CircularDependencyPlugin from 'circular-dependency-plugin';
import path from 'path';

import NodemonPlugin from "nodemon-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import { WebpackPnpExternals } from "webpack-pnp-externals";

export default {
    target: 'node',

    resolve: {
        extensions: ['.ts', '.js'],
    },

    entry: './src/index.ts',
    devtool: 'source-map',
    mode: 'development',
    output: {
        filename: "index.js",
        path: path.resolve("dist"),
    },
    externals: [
        WebpackPnpExternals({
            exclude: [/lodash-es/, /^@litbase/],
        })
    ],
    stats: "minimal",

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?|\.jsx$/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            envName: "development",
                        },
                    }
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new NodemonPlugin({
            nodeArgs: ["--trace-deprecation", "--inspect" /*, '--inspect-brk'*/]
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
} as Configuration;
